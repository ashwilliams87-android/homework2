package com.example.ashwilliams.homework2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Bundle extras = getIntent().getExtras();
        String msg="";
        if (extras != null) {
            msg = extras.getString(EXTRA_MESSAGE);
        }
        TextView myTextView = (TextView) findViewById(R.id.textView2);
        myTextView.setText(msg);
    }
}
